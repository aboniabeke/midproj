package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Person;
import kz.aitu.advancedJava.repository.PersonRepository;
import kz.aitu.advancedJava.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class    PersonController {
@Autowired
    private final PersonService personService;
@Autowired
    private PersonRepository personRepository;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping("/api/v2/users")
    public String showTables(Model model) {
        Iterable<Person> data = personRepository.findAll();
        model.addAttribute("data", data);
        return "index";
    }
    
    @GetMapping("/api/persons/{personId}")
    public ResponseEntity<?> getPerson(@PathVariable Long personId) {
        return ResponseEntity.ok(personService.getById(personId));
    }

    @GetMapping("/api/persons")
    public ResponseEntity<?> getPersons() {
        return ResponseEntity.ok(personService.getAll());
    }

    @PostMapping("/api/persons")
    public ResponseEntity<?> savePerson(@RequestBody Person person) {
        return ResponseEntity.ok(personService.create(person));
    }

    @PutMapping("/api/persons")
    public ResponseEntity<?> updatePerson(@RequestBody Person person) {
        return ResponseEntity.ok(personService.create(person));
    }

    @DeleteMapping("/api/persons/{personId}")
    public void deletePerson(@PathVariable Long personId) {
        personService.delete(personId);
    }
}